from shiny import App,ui,render
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import matplotlib.pyplot as plt

#read data from file csv
df = pd.read_csv("data/social_data.csv")

#shiny ui
app_ui = ui.page_fillable(
    ui.panel_absolute(  
        ui.card(
            ui.panel_title("Social Network"),
            ui.output_plot("plot")
        ),
        width="500px",  
        right = "25%",
        top = "25%",
        left = "35%",
        bottom = "25%",
        draggable=True,  
    ),  
)

def server(input,output,session):
    
    @output
    @render.plot
    def plot():
         # Create subplots with one row and two columns
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
    
        # Group the data by "platform" and "gender" columns and count the occurrences
        counts = df.groupby(['platform', 'gender']).size().unstack(fill_value=0)
        
        # Get the unique platforms
        platforms = counts.index.tolist()
        
        # Compute the width of each bar
        bar_width = 0.20
        
        # Generate the x-axis positions for the bars
        x = np.arange(len(platforms))
        
        # Plot the bars for males and females
        fig, ax = plt.subplots()
        ax.bar(x - bar_width/2, counts['male'], width=bar_width, color='b', label='Male')
        ax.bar(x + bar_width/2, counts['female'], width=bar_width, color='r', label='Female')
        
        # Set the tick positions and labels
        ax.set_xticks(x)
        ax.set_xticklabels(platforms)
        
        # Set the axis labels
        ax.set_xlabel('Platform')
        ax.set_ylabel('Count')
        
        # Add a legend
        ax.legend()
        
        return fig



app = App(app_ui,server)




