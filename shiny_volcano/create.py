# from shiny.express import ui,input,render
# from ipyleaflet import Map ,Marker,CircleMarker,GeoData
from shinywidgets import output_widget,render_widget
import pandas as pd
import numpy as np
import plotly.graph_objs as go




def create_map(latitude, longitude, country, volcano_data):
    fig = go.FigureWidget(
        data=[
            go.Scattermapbox(
                lat=latitude,
                lon=longitude,
                mode='markers',
                marker=go.scattermapbox.Marker(
                    size=5,
                    sizemin=1.5,
                    sizemode='diameter',
                    color='#0d6aff',
                    opacity=0.5
                ),
                text=country + ": " + volcano_data,
                hoverinfo='text'
            ),
            go.Scattermapbox(
                lat=latitude,
                lon=longitude,
                mode='markers',
                marker=go.scattermapbox.Marker(
                    size=15,
                    sizemin=1.5,
                    sizemode='diameter',
                    color='#ff1d0d',
                    opacity=0.5
                ),
                text=country + ": " + volcano_data,
                hoverinfo='text'
            )
        ],
        layout=dict(
            autosize=True,
            hovermode='closest',
            height=1000,
            width=1200,
            margin={"r": 0, "t": 0, "l": 0, "b": 0},
            mapbox=dict(
                style='open-street-map',
                zoom=0.9
            ),
            showlegend=False,
        )
    )
    return fig

