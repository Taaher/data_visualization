from shiny import App,ui,render,reactive
from shinywidgets import output_widget,register_widget,reactive_read
import pandas as pd

from create import create_map


app_ui = ui.page_fluid(
    # ui.page_navbar(  
    # title="Tz | Shiny and Python",  
    # id="page",
    
    # ),  
    ui.page_fillable(
    ui.layout_columns(  
        ui.card(  
            ui.card_header("Visualization Data"),
            ui.p(
                ui.output_text("loc"),
            ),
        ),  
    )  
),
    ui.panel_main(
        output_widget("map",width='100%',height="100%")
    ),  
)
data = pd.read_csv('data/volcano.csv')
df = pd.DataFrame(data)

def server(input,output,session):
    map = create_map(df.latitude, df.longitude, df.region, df.volcano_name)
    
    register_widget("map", map) 
    

    @render.text
    def loc():
        return f"Volcano Explorer Map! This map allows you to explore various volcanoes around the world. Simply click on a volcano marker on the map to view detailed information about it."
    
    @reactive.Effect
    def _():
        pass

    # @reactive.Effect
    # @reactive_read(names="get_",widget="map")
    # def on_change_data(event):
    #     print(event)
    #     pass
        
    
    

app = App(app_ui,server)